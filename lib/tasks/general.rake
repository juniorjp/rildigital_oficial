namespace :general do

  
  task :migrate => :environment do
    Rake::Task["db:migrate"].execute
    Rake::Task["db:seed"].execute
    Rake::Task["country:load"].execute
    Rake::Task["users:create_top"].execute
    Rake::Task["general:generate_missing_description"].execute

  end

  task :generate_missing_description => :environment do
    descriptions = TransferHistory::DESCRIPTIONS
    descriptions.each_with_index{|description, index|
      database_description = TransferDescription.find_by_description description
      if database_description.nil?
        puts "generating description: #{description}"
        TransferDescription.create!(id: (index + 1), description: description)
      end

    }
  end

  task :create_admin, [:username, :email, :password] => :environment do |t, args|
    a = Admin.create(username: args.username, email: args.email, password: args.password)

  end

  task :create_subjects => :environment do |t, args|
    ["Financial", "Network", "Other"].each do |subject|
      SubjectKind.create(name: subject)
    end
    puts "done!"

  end




end
    
