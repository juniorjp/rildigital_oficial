task :kill_postgres_connections => :environment do
  db_name = "ads"
  sh = <<EOF
ps xa \
  | grep postgres: \
  | grep #{db_name} \
  | grep -v grep \
  | awk '{print $1}' \
  | xargs kill
EOF
  puts `#{sh}`
end

task :pg_backup => [:environment] do
  #stamp the filename
  datestamp = Time.now.strftime("%Y-%m-%d_%H-%M-%S")

  #drop it in the db/backups directory temporarily
  backup_file = "/home/sample/public/backups/sample_#{datestamp}_dump.sql.gz"

  #dump the backup and zip it up
  sh "pg_dump -h localhost -U kilua sample | gzip -c > #{backup_file}"
     
end
  
