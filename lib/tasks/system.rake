
namespace :unicorn do
  #modify the pid address for current app:
  unicorn_pid = "/home/deploy/apps/shared/pids/unicorn.rildigital_oficial.pid"
  desc "reload unicorn"
  task :reload do
    puts "Unicorn is reloading #{unicorn_pid}"
    old_pid = File.open(unicorn_pid).first
    system "kill -s USR2 #{old_pid}"    
    puts "unicorn restarted"
  end
   
end
